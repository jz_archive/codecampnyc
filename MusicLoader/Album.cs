﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicLoader
{
    public class Album : DocumentBase
    {
        public string Name { get; set; }

        public int Year { get; set; }

        public string Genre { get; set; }

        public IList<Track> Tracks { get; set; }

        public string ArtistId { get; set; }
    }
}
