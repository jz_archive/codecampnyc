﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicLoader
{
    public abstract class DocumentBase
    {
        [JsonIgnore]
        public ObjectId Id { get; set; }

        [BsonIgnore]
        public string Type { get { return this.GetType().Name.ToLower(); } }
    }
}
