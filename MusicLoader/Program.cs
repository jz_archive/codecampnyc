﻿using Couchbase;
using Couchbase.Configuration;
using Id3;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Couchbase.Extensions;
using Enyim.Caching.Memcached;
using Couchbase.Management;

namespace MusicLoader
{
    class Program
    {
        private static CouchbaseClient _couchbase;
        private static MongoDatabase _mongo;

        static Program()
        {
            if (_mongo == null)
            {
                var server = new MongoClient().GetServer();
                _mongo = server.GetDatabase("codecamp");
                _mongo.DropCollection("artist");
                _mongo.DropCollection("album");
            }

            if (_couchbase == null)
            {
                var cfg = new CouchbaseClientConfiguration();
                cfg.Bucket = "codecamp";
                cfg.Urls.Add(new Uri("http://localhost:8091/pools"));
                _couchbase = new CouchbaseClient(cfg);
                var cluster = new CouchbaseCluster(cfg);
                cluster.FlushBucket(cfg.Bucket);
            }


        }

        static void Main(string[] args)
        {
            var musicDir = new DirectoryInfo(ConfigurationManager.AppSettings["MusicDirectory"]);

            foreach (var artistDir in musicDir.GetDirectories())
            {
                var artist = new Artist
                {
                    Name = artistDir.Name
                };

                createMongoDoc(artist);
                createCouchbaseDoc(artist);

                foreach (var albumDir in artistDir.GetDirectories())
                {
                    var trackFiles = albumDir.GetFiles("*.mp3");
                    if (trackFiles.Count() == 0) continue;

                    var firstTrackName = trackFiles.First().FullName;
                    var firstTrackMp3Tag = new Mp3File(firstTrackName).GetTag(Id3TagFamily.FileStartTag);

                    var album = new Album
                    {
                        Name = albumDir.Name,
                        Genre = firstTrackMp3Tag.Genre.Value,
                        Tracks = new List<Track>(),
                        ArtistId = artist.Id.ToString(),
                        Year = ! string.IsNullOrEmpty(firstTrackMp3Tag.Year.Value) ? 
                                    DateTime.Parse(firstTrackMp3Tag.Year.Value).Year :
                                    DateTime.Now.Year //hack
                    };

                    foreach (var trackFile in trackFiles)
                    {
                        Console.WriteLine(trackFile.FullName);
                        using (var mp3 = new Mp3File(trackFile.FullName))
                        {
                            var tag = mp3.GetTag(Id3TagFamily.FileStartTag);
                            if (tag == null) continue;
                            album.Tracks.Add(new Track
                            {
                                Name = tag.Title.Value,
                                FileSize = (int)trackFile.Length,
                                Duration = mp3.Audio.Duration
                            });
                        }
                    }

                    createMongoDoc(album);
                    createCouchbaseDoc(album);
                }
            }

        }

        private static void createMongoDoc(DocumentBase doc)
        {
            _mongo.GetCollection(doc.Type).Insert<DocumentBase>(doc);
        }

        private static void createCouchbaseDoc(DocumentBase doc)
        {
            _couchbase.ExecuteStoreJson(StoreMode.Set, doc.Id.ToString(), doc);
        }
    }
}
