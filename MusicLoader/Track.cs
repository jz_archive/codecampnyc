﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicLoader
{
    public class Track
    {
        public string Name { get; set; }

        public TimeSpan Duration { get; set; }

        public int FileSize { get; set; }
    }
}
